/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.reark.rxgithubapp.shared.view;



import java.util.ArrayList;
import java.util.List;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import io.reark.rxgithubapp.shared.ResourceTable;
import io.reark.rxgithubapp.shared.pojo.GitHubRepository;



import ohos.agp.components.*;
import ohos.app.Context;


public class RepositoriesAdapter extends BaseItemProvider {
    Context context;
    private final List<GitHubRepository> gitHubRepositories = new ArrayList<>(10);


    public RepositoriesAdapter(List<GitHubRepository> gitHubRepositories, Context context) {
        this.gitHubRepositories.addAll(gitHubRepositories);
        this.context=context;
    }


    @Override
    public int getCount() {
        return gitHubRepositories.size();
    }

    public GitHubRepository getItem(int position) {
        return gitHubRepositories.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        RepositoryViewHolder holder = null;
        if (component == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_repository_item, null, false);
            holder = new RepositoryViewHolder(cpt);
            cpt.setTag(holder);
        } else {
            cpt = component;
            holder = (RepositoryViewHolder) cpt.getTag();

        }
        GitHubRepository gitHubRepository = gitHubRepositories.get(position);
        holder.titleTextView.setText(gitHubRepository.getName());
        Glide.with(context)
                .load(gitHubRepository.getOwner().getAvatarUrl())
                .skipMemoryCache(true).fitCenter()
                .into(holder.avatarImageView);

        return cpt;
    }


    public void set(List<GitHubRepository> gitHubRepositories) {
        this.gitHubRepositories.clear();
        this.gitHubRepositories.addAll(gitHubRepositories);
        notifyDataChanged();
    }

    public static class RepositoryViewHolder {


        public final Image avatarImageView;


        public final Text titleTextView;

        public RepositoryViewHolder(Component view) {
            avatarImageView = (Image) view.findComponentById(ResourceTable.Id_avatar_image_view);
            titleTextView = (Text) view.findComponentById(ResourceTable.Id_repository_name);
        }
    }
}
