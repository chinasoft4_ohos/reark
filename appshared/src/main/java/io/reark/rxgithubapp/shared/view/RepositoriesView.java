/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.reark.rxgithubapp.shared.view;


import io.reark.rxgithubapp.shared.ResourceTable;
import io.reark.rxgithubapp.shared.pojo.GitHubRepository;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Collections;
import java.util.List;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class RepositoriesView extends StackLayout {

    private Text statusText;

    private ListContainer repositoriesListView;

    private RepositoriesAdapter repositoriesAdapter;

    public RepositoriesView(Context context) {
        super(context, null);
    }

    public RepositoriesView(Context context, AttrSet attrs) {
        super(context, attrs);
        onFinishInflate(context);
    }

    protected void onFinishInflate(Context context) {


        TextField editText = (TextField) findComponentById(ResourceTable.Id_repositories_search);
        editText.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {

            }
        });

        statusText = (Text) findComponentById(ResourceTable.Id_repositories_status_text);

        repositoriesAdapter = new RepositoriesAdapter(Collections.emptyList(),context);

        repositoriesListView = (ListContainer) findComponentById(ResourceTable.Id_repositories_list_view);
        repositoriesListView.setLongClickable(false);
        repositoriesListView.setItemProvider(repositoriesAdapter);
    }

    private void setRepositories( final List<GitHubRepository> repositories) {
        checkNotNull(repositories);
        checkNotNull(repositoriesAdapter);

        repositoriesAdapter.set(repositories);
    }




    private void setNetworkRequestStatusText( final String networkRequestStatusText) {
        checkNotNull(networkRequestStatusText);
        checkNotNull(statusText);

        statusText.setText(networkRequestStatusText);
    }


}
