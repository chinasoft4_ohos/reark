/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.reark.rxgithubapp.shared.view;

import com.bumptech.glide.Glide;
import io.reark.rxgithubapp.shared.ResourceTable;
import io.reark.rxgithubapp.shared.pojo.GitHubRepository;
import ohos.agp.components.*;
import ohos.app.Context;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class RepositoryView extends StackLayout implements Component.BindStateChangedListener {
    private Text titleTextView;
    private Text stargazersTextView;
    private Text forksTextView;
    private Image avatarImageView;




    public RepositoryView(Context context) {
        super(context);
    }

    public RepositoryView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
        setBindStateChangedListener(this);
    }


    protected void init() {

        titleTextView = (Text) findComponentById(ResourceTable.Id_widget_layout_title);
        stargazersTextView = (Text) findComponentById(ResourceTable.Id_widget_layout_stargazers);
        forksTextView = (Text) findComponentById(ResourceTable.Id_widget_layout_forks);
        avatarImageView = (Image) findComponentById(ResourceTable.Id_widget_avatar_image_view_1);
    }

    private void setRepository( final GitHubRepository repository) {
        checkNotNull(repository);

        titleTextView.setText(repository.getName());
        stargazersTextView.setText("stars: " + repository.getStargazersCount());
        forksTextView.setText("forks: " + repository.getForksCount());
      Glide.with(getContext())
                .load(repository.getOwner().getAvatarUrl())
                .fitCenter().skipMemoryCache(true)
                .into(avatarImageView);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {

    }


}
