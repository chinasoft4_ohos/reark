/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.reark.rxgithubapp.shared.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GitHubRepositorySearchResults {

    private List<GitHubRepository> items;

    public GitHubRepositorySearchResults(final List<GitHubRepository> items) {
        this.items = new ArrayList<>(get(items));
    }


    public List<GitHubRepository> getItems() {
        return Collections.unmodifiableList(items);
    }
    public static <T> T get(final T object) {
        checkNotNull(object);

        return object;
    }
    public static void checkNotNull(final Object object,final String message) {
        if (object == null) {
            throw new NullPointerException(message);
        }
    }

    public static void checkNotNull(final Object object) {
        checkNotNull(object, "Object cannot be null.");
    }
}