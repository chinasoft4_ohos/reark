/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.reark.rxgithubapp;

import io.reark.reark.utils.Log;
import io.reark.rxgithubapp.advanced.data.DbUtils;
import io.reark.rxgithubapp.advanced.data.UserSettings;
import io.reark.rxgithubapp.shared.ResourceTable;
import io.reark.rxgithubapp.shared.network.NetworkApi;
import io.reark.rxgithubapp.shared.pojo.GitHubRepository;
import ohos.aafwk.ability.AbilityPackage;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class RxGitHubApp extends AbilityPackage {
    private static final String TAG = RxGitHubApp.class.getSimpleName();
    private static final int DEFAULT_REPOSITORY_ID = 15491874;

    @Override
    public void onInitialize() {
        super.onInitialize();
        UserSettings userSettings = DbUtils.getWidget(this);
        if (!userSettings.getName().equals("")) {
            Log.d(TAG, "onInitialize" + "获取保存数据" + userSettings.toString());
            System.out.println("onInitialize" + "获取保存数据" + userSettings.toString());
            return;
        }
        Log.d(TAG, "onInitialize" + "开始网络请求数据");
        System.out.println("onInitialize" + "开始网络请求数据");
        String baseUrl = "";
        try {
            baseUrl =  getResourceManager().getElement(ResourceTable.String_base_url).getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            Log.d(TAG,e.getMessage());
        }
        Call<GitHubRepository> s = new NetworkApi(baseUrl).getRepository(DEFAULT_REPOSITORY_ID);
        s.enqueue(new Callback<GitHubRepository>() {
            @Override
            public void onResponse(Call<GitHubRepository> call, Response<GitHubRepository> response) {
                GitHubRepository repository = response.body();
                UserSettings userSettings = new UserSettings();
                userSettings.setId((long) repository.getId());
                userSettings.setName(repository.getName());
                userSettings.setStargazersCount(repository.getStargazersCount());
                userSettings.setForksCount(repository.getForksCount());
                userSettings.setAvatarUrl(repository.getOwner().getAvatarUrl());
                DbUtils.setWidget(userSettings, getContext());
                Log.d(TAG, "onInitialize" + "网络请求数据" + userSettings.toString());
            }

            @Override
            public void onFailure(Call<GitHubRepository> call, Throwable throwable) {
                Log.d(TAG, "onFailure" + "网络请求数据" + throwable.getMessage());
            }
        });
    }


}
