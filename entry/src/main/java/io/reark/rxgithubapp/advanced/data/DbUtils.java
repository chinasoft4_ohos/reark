/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.reark.rxgithubapp.advanced.data;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.data.preferences.Preferences;

import java.util.List;

/**
 * 卡片数据库操作
 *
 * @since 2021-08-06
 */
public class DbUtils {
    /**
     * 数据库别名
     */
    public static final String A_LIAS = "GithubDB";
    /**
     * 数据库名称
     */
    public static final String DB = "GithubDB.db";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final long INVALID = -1L;
    private static Preferences preferences;

    private DbUtils() {
    }

    private static Preferences getPreferences(Context context) {
        if (preferences == null) {
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            preferences = databaseHelper.getPreferences("app_preference.xml");
        }
        return preferences;
    }

    /**
     * setWidget数据
     *
     * @param userSettings userSettings
     * @param context mContext
     */
    public static void setWidget(UserSettings userSettings, Context context) {
        getPreferences(context).putLong(ID, userSettings.getId());
        getPreferences(context).putString(NAME, userSettings.getName());
        getPreferences(context).putInt("forksCount", userSettings.getForksCount());
        getPreferences(context).putInt("stargazersCount", userSettings.getStargazersCount());
        getPreferences(context).putString("avatarUrl", userSettings.getAvatarUrl());
        getPreferences(context).flush();
    }

    /**
     * 获取Widget 数据
     *
     * @param context mContext
     * @return Form
     */
    public static UserSettings getWidget(Context context) {
        UserSettings userSettings = new UserSettings();
        Long id = getPreferences(context).getLong(ID, INVALID);
        userSettings.setId(id);
        String name = getPreferences(context).getString(NAME, "");
        userSettings.setName(name);
        int forksCount = getPreferences(context).getInt("forksCount", 0);
        userSettings.setForksCount(forksCount);
        int stargazersCount = getPreferences(context).getInt("stargazersCount", 0);
        userSettings.setStargazersCount(stargazersCount);
        String avatarUrl = getPreferences(context).getString("avatarUrl", "");
        userSettings.setAvatarUrl(avatarUrl);
        getPreferences(context).flush();
        return userSettings;
    }

    /**
     * 保存json 数据到数据库
     *
     * @param name 搜索的字符串
     * @param form 数据库保存的对象
     * @param connect connect
     */
    public static void insertJson(String name, FormJson form, OrmContext connect) {
        OrmPredicates ormPredicates = connect.where(FormJson.class).equalTo(NAME, name);
        List<FormJson> json = connect.query(ormPredicates);
        if (json.size() > 0) {
            return;
        } else {
            connect.insert(form);
        }
        connect.flush();
    }

    /**
     * 通过name查询数据库数据
     *
     * @param name 搜索的字符串
     * @param connect connect
     * @return 返回搜索的对象
     */
    public static List<FormJson> query(String name, OrmContext connect) {
        OrmPredicates ormPredicates = connect.where(FormJson.class).equalTo(NAME, name);
        List<FormJson> list = connect.query(ormPredicates);
        connect.flush();
        return list;
    }

    /**
     * 数据插入数据库
     *
     * @param userSettings UserSettings
     * @param connect OrmContext
     */
    public static void insert(UserSettings userSettings, OrmContext connect) {
        connect.insert(userSettings);
        connect.flush();
    }

    /**
     * 删除指定数据
     *
     * @param formId long
     * @param connect OrmContext
     */
    public static void delete(long formId, OrmContext connect) {
        OrmPredicates where = connect.where(UserSettings.class).equalTo("id", formId);
        List<UserSettings> query = connect.query(where);
        if (!query.isEmpty()) {
            connect.delete(query.get(0));
            connect.flush();
        }
    }
}
